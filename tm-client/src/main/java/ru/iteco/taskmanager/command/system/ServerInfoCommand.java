package ru.iteco.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.command.AbstractCommand;

@Component
public class ServerInfoCommand extends AbstractCommand {

    @Autowired
    private ISessionService sessionService;
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @Override
    public @NotNull String command() {
        return "server-info";
    }

    @Override
    public @NotNull String description() {
        return "  -  show server information";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("HOST: localhost");
        System.out.println("PORT: " + sessionEndpoint.getPort());
    }
}
