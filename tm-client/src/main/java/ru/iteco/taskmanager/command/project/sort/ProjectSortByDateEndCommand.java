package ru.iteco.taskmanager.command.project.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Component
public class ProjectSortByDateEndCommand extends AbstractCommand {

    @Autowired
    private IUserEndpoint userEndpoint;
    @Autowired
    private IProjectEndpoint projectEndpoint;
    @Autowired
    private ISessionService sessionService;

    @Override
    public String command() {
	return "project-sort-date-end";
    }

    @Override
    public String description() {
	return "  -  find all project and sort them by date end";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;
	@Nullable
	final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
	if (userDTO == null)
	    return;

	@Nullable
	final List<Project> tempList = ProjectDTOConvertUtil.DTOsToProjects(projectEndpoint.findAllProject(sessionDTO));

	@NotNull
	final Comparator<Project> compareByDateEnd = (Project o1, Project o2) -> o1.getDateEnd()
		.compareTo(o2.getDateEnd());
	Collections.sort(tempList, compareByDateEnd);

	for (int i = 0, j = 1; i < tempList.size(); i++) {
	    if (tempList.get(i).getOwnerId().equals(userDTO.getId())) {
		System.out.println("[Project " + (j++) + "]");
		System.out.println(tempList.get(i));
	    }
	}
    }
}
