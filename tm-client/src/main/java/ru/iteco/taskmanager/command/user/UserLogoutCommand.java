package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.ITerminalService;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Component
public class UserLogoutCommand extends AbstractCommand {

    @Autowired
    private ISessionService sessionService;
    @Autowired
    private ITerminalService terminalService;
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @Override
    public String command() {
	return "logout";
    }

    @Override
    public String description() {
	return "  -  user logout";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;
	sessionEndpoint.removeSession(sessionDTO);
	sessionService.setSession(null);
	System.out.println("Done");
	terminalService.get("login").execute();
    }

}
