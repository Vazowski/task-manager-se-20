package ru.iteco.taskmanager.command.serialize.load;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Component
public class DataFromBinCommand extends AbstractCommand {

    @Autowired
    private IDomainEndpoint domainEndpoint;
    @Autowired
    private ISessionService sessionService;

    @Override
    public @NotNull String command() {
	return "data-from-bin";
    }

    @Override
    public @NotNull String description() {
	return "  -  load data from binary file";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;

	if (domainEndpoint.loadData(sessionDTO) != null) {
	    sessionService.setSession(null);
	    System.out.println("Done");
	}
    }
}
