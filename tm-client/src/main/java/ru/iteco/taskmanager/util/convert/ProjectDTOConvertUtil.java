package ru.iteco.taskmanager.util.convert;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.ProjectDTO;

public class ProjectDTOConvertUtil {

    @Nullable
    public static ProjectDTO projectToDTO(@Nullable final Project project) {
	if (project == null)
	    return null;

	@NotNull
	final ProjectDTO projectDTO = new ProjectDTO();
	projectDTO.setId(project.getId());
	projectDTO.setOwnerId(project.getOwnerId());
	projectDTO.setName(project.getName());
	projectDTO.setDescription(project.getDescription());
	projectDTO.setDateCreated(project.getDateCreated());
	projectDTO.setDateBegin(project.getDateBegin());
	projectDTO.setDateEnd(project.getDateEnd());
	projectDTO.setType("Project");
	projectDTO.setReadinessStatus(project.getReadinessStatus());
	return projectDTO;
    }

    @Nullable
    public static List<ProjectDTO> projectsToDTO(@Nullable final List<Project> listProject) {
	if (listProject == null || listProject.isEmpty())
	    return null;
	@NotNull
	final List<ProjectDTO> listProjectsDTO = new ArrayList<>();
	for (Project project : listProject) {
	    listProjectsDTO.add(projectToDTO(project));
	}
	return listProjectsDTO;
    }

    @Nullable
    public static Project DTOToProject(@Nullable final ProjectDTO projectDTO) {
	if (projectDTO == null)
	    return null;

	@Nullable
	final Project project = new Project();
	project.setId(projectDTO.getId());
	project.setOwnerId(projectDTO.getOwnerId());
	project.setName(projectDTO.getName());
	project.setDescription(projectDTO.getDescription());
	project.setDateCreated(projectDTO.getDateCreated());
	project.setDateBegin(projectDTO.getDateBegin());
	project.setDateEnd(projectDTO.getDateEnd());
	project.setReadinessStatus(projectDTO.getReadinessStatus());
	return project;
    }

    @Nullable
    public static List<Project> DTOsToProjects(@Nullable final List<ProjectDTO> listProjectDTOs) {
	if (listProjectDTOs == null || listProjectDTOs.isEmpty())
	    return null;
	@NotNull
	final List<Project> listProjects = new ArrayList<>();
	for (ProjectDTO projectDTO : listProjectDTOs) {
	    listProjects.add(DTOToProject(projectDTO));
	}
	return listProjects;
    }
}
