package ru.iteco.taskmanager.bootstrap;

import java.util.Scanner;
import java.util.Set;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.ITerminalService;
import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.command.AbstractCommand;

@Component
public class Bootstrap {

    @Autowired
    private ApplicationContext applicationContext;

    @NotNull
    private Scanner scanner;
    @NotNull
    private String inputCommand;

    @Autowired
    private ITerminalService terminalService;
    @Autowired
    private ISessionService sessionService;

    @Autowired
    private IUserEndpoint userEndpoint;
    @Autowired
    private IProjectEndpoint projectEndpoint;
    @Autowired
    private ITaskEndpoint taskEndpoint;
    @Autowired
    private ISessionEndpoint sessionEndpoint;
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("ru.iteco.taskmanager")
            .getSubTypesOf(AbstractCommand.class);

    public void init() {
        scanner = new Scanner(System.in);
        try {
            registerCommand(classes);
            terminalService.get("login").execute();
            System.out.println("Enter command (type help for more information)");

            while (true) {
                System.out.print("> ");
                inputCommand = scanner.nextLine();
                if (!terminalService.getCommands().containsKey(inputCommand)) {
                    System.out.println("Command doesn't exist");
                } else {
                    terminalService.get(inputCommand).execute();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerCommand(@Nullable final Set<Class<? extends AbstractCommand>> classes) throws Exception {
        if (classes == null)
            return;
        for (@Nullable final Class clazz : classes) {
            if (clazz == null || !AbstractCommand.class.isAssignableFrom(clazz))
                continue;
            @NotNull
            AbstractCommand command = (AbstractCommand) applicationContext.getBean(clazz);
            registerCommand(command);
        }
    }

    private void registerCommand(@Nullable final AbstractCommand command) {
        if (command == null)
            return;
        terminalService.put(command.command(), command);
    }
}
