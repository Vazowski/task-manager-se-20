package ru.iteco.taskmanager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.endpoint.DomainEndpointService;
import ru.iteco.taskmanager.endpoint.ProjectEndpointService;
import ru.iteco.taskmanager.endpoint.SessionEndpointService;
import ru.iteco.taskmanager.endpoint.TaskEndpointService;
import ru.iteco.taskmanager.endpoint.UserEndpointService;

@Configuration
public class EndpointProducer {

    @Bean
    public IUserEndpoint userEndpoint() {
	return new UserEndpointService().getUserEndpointPort();
    }

    @Bean
    public IDomainEndpoint domainEndpoint() {
	return new DomainEndpointService().getDomainEndpointPort();
    }

    @Bean
    public IProjectEndpoint projectEndpoint() {
	return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    public ITaskEndpoint taskEndpoint() {
	return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    public ISessionEndpoint sessionEndpoint() {
	return new SessionEndpointService().getSessionEndpointPort();
    }
}
