package ru.iteco.taskmanager.api;

import org.springframework.stereotype.Component;
import ru.iteco.taskmanager.api.endpoint.Session;

@Component
public interface ISessionService {

    void setSession(Session session);

    Session getSession();
}
