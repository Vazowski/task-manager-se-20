
package ru.iteco.taskmanager.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.iteco.taskmanager.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FindById_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "findById");
    private final static QName _FindByIdResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "findByIdResponse");
    private final static QName _GetPort_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "getPort");
    private final static QName _GetPortResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "getPortResponse");
    private final static QName _MergeSession_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "mergeSession");
    private final static QName _MergeSessionResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "mergeSessionResponse");
    private final static QName _RemoveSession_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "removeSession");
    private final static QName _RemoveSessionResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "removeSessionResponse");
    private final static QName _SignSession_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "signSession");
    private final static QName _SignSessionResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "signSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.iteco.taskmanager.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindById }
     * 
     */
    public FindById createFindById() {
        return new FindById();
    }

    /**
     * Create an instance of {@link FindByIdResponse }
     * 
     */
    public FindByIdResponse createFindByIdResponse() {
        return new FindByIdResponse();
    }

    /**
     * Create an instance of {@link GetPort }
     * 
     */
    public GetPort createGetPort() {
        return new GetPort();
    }

    /**
     * Create an instance of {@link GetPortResponse }
     * 
     */
    public GetPortResponse createGetPortResponse() {
        return new GetPortResponse();
    }

    /**
     * Create an instance of {@link MergeSession }
     * 
     */
    public MergeSession createMergeSession() {
        return new MergeSession();
    }

    /**
     * Create an instance of {@link MergeSessionResponse }
     * 
     */
    public MergeSessionResponse createMergeSessionResponse() {
        return new MergeSessionResponse();
    }

    /**
     * Create an instance of {@link RemoveSession }
     * 
     */
    public RemoveSession createRemoveSession() {
        return new RemoveSession();
    }

    /**
     * Create an instance of {@link RemoveSessionResponse }
     * 
     */
    public RemoveSessionResponse createRemoveSessionResponse() {
        return new RemoveSessionResponse();
    }

    /**
     * Create an instance of {@link SignSession }
     * 
     */
    public SignSession createSignSession() {
        return new SignSession();
    }

    /**
     * Create an instance of {@link SignSessionResponse }
     * 
     */
    public SignSessionResponse createSignSessionResponse() {
        return new SignSessionResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link AbstractDTO }
     * 
     */
    public AbstractDTO createAbstractDTO() {
        return new AbstractDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "findById")
    public JAXBElement<FindById> createFindById(FindById value) {
        return new JAXBElement<FindById>(_FindById_QNAME, FindById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "findByIdResponse")
    public JAXBElement<FindByIdResponse> createFindByIdResponse(FindByIdResponse value) {
        return new JAXBElement<FindByIdResponse>(_FindByIdResponse_QNAME, FindByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPort }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "getPort")
    public JAXBElement<GetPort> createGetPort(GetPort value) {
        return new JAXBElement<GetPort>(_GetPort_QNAME, GetPort.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPortResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "getPortResponse")
    public JAXBElement<GetPortResponse> createGetPortResponse(GetPortResponse value) {
        return new JAXBElement<GetPortResponse>(_GetPortResponse_QNAME, GetPortResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "mergeSession")
    public JAXBElement<MergeSession> createMergeSession(MergeSession value) {
        return new JAXBElement<MergeSession>(_MergeSession_QNAME, MergeSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "mergeSessionResponse")
    public JAXBElement<MergeSessionResponse> createMergeSessionResponse(MergeSessionResponse value) {
        return new JAXBElement<MergeSessionResponse>(_MergeSessionResponse_QNAME, MergeSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "removeSession")
    public JAXBElement<RemoveSession> createRemoveSession(RemoveSession value) {
        return new JAXBElement<RemoveSession>(_RemoveSession_QNAME, RemoveSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "removeSessionResponse")
    public JAXBElement<RemoveSessionResponse> createRemoveSessionResponse(RemoveSessionResponse value) {
        return new JAXBElement<RemoveSessionResponse>(_RemoveSessionResponse_QNAME, RemoveSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "signSession")
    public JAXBElement<SignSession> createSignSession(SignSession value) {
        return new JAXBElement<SignSession>(_SignSession_QNAME, SignSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "signSessionResponse")
    public JAXBElement<SignSessionResponse> createSignSessionResponse(SignSessionResponse value) {
        return new JAXBElement<SignSessionResponse>(_SignSessionResponse_QNAME, SignSessionResponse.class, null, value);
    }

}
