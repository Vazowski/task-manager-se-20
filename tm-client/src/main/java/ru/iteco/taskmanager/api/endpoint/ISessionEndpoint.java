package ru.iteco.taskmanager.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-27T21:02:55.382+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "ISessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ISessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/ISessionEndpoint/mergeSessionRequest", output = "http://endpoint.api.taskmanager.iteco.ru/ISessionEndpoint/mergeSessionResponse")
    @RequestWrapper(localName = "mergeSession", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.MergeSession")
    @ResponseWrapper(localName = "mergeSessionResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.MergeSessionResponse")
    public void mergeSession(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/ISessionEndpoint/findByIdRequest", output = "http://endpoint.api.taskmanager.iteco.ru/ISessionEndpoint/findByIdResponse")
    @RequestWrapper(localName = "findById", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.FindById")
    @ResponseWrapper(localName = "findByIdResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.FindByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.SessionDTO findById(
        @WebParam(name = "session", targetNamespace = "")
        java.lang.String session
    );

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/ISessionEndpoint/signSessionRequest", output = "http://endpoint.api.taskmanager.iteco.ru/ISessionEndpoint/signSessionResponse")
    @RequestWrapper(localName = "signSession", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.SignSession")
    @ResponseWrapper(localName = "signSessionResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.SignSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.SessionDTO signSession(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/ISessionEndpoint/removeSessionRequest", output = "http://endpoint.api.taskmanager.iteco.ru/ISessionEndpoint/removeSessionResponse")
    @RequestWrapper(localName = "removeSession", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.RemoveSession")
    @ResponseWrapper(localName = "removeSessionResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.RemoveSessionResponse")
    public void removeSession(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/ISessionEndpoint/getPortRequest", output = "http://endpoint.api.taskmanager.iteco.ru/ISessionEndpoint/getPortResponse")
    @RequestWrapper(localName = "getPort", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.GetPort")
    @ResponseWrapper(localName = "getPortResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.GetPortResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String getPort();
}
