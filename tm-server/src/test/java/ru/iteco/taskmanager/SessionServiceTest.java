package ru.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.iteco.taskmanager.api.service.ISessionService;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.config.AppTestConfiguration;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
public class SessionServiceTest extends Assert {

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private IUserService userService;

    @Nullable
    private User user = new User();

    @Nullable
    private Session session = new Session();

    @NotNull
    private static final String LOGIN = "test";

    @NotNull
    private static final String PASSWORD_HASH = "81dc9bdb52d04dc20036dbd8313ed055";

    @Before
    public void init() {
        user.setLogin(LOGIN);
        user.setPasswordHash(PASSWORD_HASH);
        userService.save(user);
        session.setUser(user);
    }

    @Test
    public void findById() {
        @Nullable
        final Session testSession = sessionService.findById(session.getId());
        assertNotNull(testSession);
    }

    @Test
    public void remove() {
        sessionService.remove(session.getId());
        assertNotNull(session);
    }
}
