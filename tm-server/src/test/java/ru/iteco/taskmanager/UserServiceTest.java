package ru.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.config.AppTestConfiguration;
import ru.iteco.taskmanager.entity.User;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest extends Assert {

    @NotNull
    private static final String LOGIN = "test";

    @NotNull
    private static final String PASSWORD_HASH = "81dc9bdb52d04dc20036dbd8313ed055";

    @Autowired
    private IUserService userService;

    @Nullable
    private User user = new User();

    @Before
    public void init() {
        user.setLogin(LOGIN);
        user.setPasswordHash(PASSWORD_HASH);
        userService.save(user);
    }

    @Test
    public void findByLogin() {
        @Nullable
        final User newUser = userService.findByLogin(LOGIN);
        assertNotNull(newUser);
    }

    @Test
    public void findAll() {
        @Nullable
        final List<User> userList = userService.findAll();
        assertEquals(userList.size(), 1);
    }

    @After
    public void close() {
        userService = null;
    }
}
