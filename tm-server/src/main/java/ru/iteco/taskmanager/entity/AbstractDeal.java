package ru.iteco.taskmanager.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;
import ru.iteco.taskmanager.enumerate.ReadinessStatus;
import ru.iteco.taskmanager.util.DateUtil;

@Getter
@Setter
@MappedSuperclass
public class AbstractDeal extends AbstractEntity {

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String dateCreated = DateUtil.getDate(new Date().toString());

    @Column
    private String dateBegin;

    @Column
    private String dateEnd;

    @Enumerated(value = EnumType.STRING)
    private ReadinessStatus readinessStatus;
}
