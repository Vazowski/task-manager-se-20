package ru.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.iteco.taskmanager.bootstrap.Bootstrap;
import ru.iteco.taskmanager.config.AppConfiguration;

public class App {
    public static void main(final String[] args) {
        @Nullable
        final ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
        @NotNull
        final Bootstrap bootstrap = context.getBean("bootstrap", Bootstrap.class);
        bootstrap.init();
    }
}
