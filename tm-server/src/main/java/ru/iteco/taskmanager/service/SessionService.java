package ru.iteco.taskmanager.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.taskmanager.api.service.ISessionService;
import ru.iteco.taskmanager.bootstrap.Bootstrap;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.repository.SessionRepository;

@Getter
@Setter
@Service
@Transactional
@NoArgsConstructor
public class SessionService extends AbstractService implements ISessionService {

    @Autowired
    private SessionRepository sessionRepository;

    public void save(@Nullable final Session session) {
    	sessionRepository.save(session);
    }

    @Nullable
    public Session findById(@Nullable final String id) {
    	return sessionRepository.getOne(id);
    }

    public void remove(@NotNull final String id) {
		sessionRepository.delete(findById(id));
	}

	@NotNull
	public String getPort() {
        return Bootstrap.port;
    }
}
