package ru.iteco.taskmanager.service;

import java.util.List;

import javax.persistence.NoResultException;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.repository.UserRepository;

@Getter
@Setter
@Service
@Transactional
@NoArgsConstructor
public class UserService extends AbstractService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    public void save(@NotNull final User user) {
		assert userRepository != null;
		userRepository.save(user);
    }

    public User findById(@NotNull final String id) {
        try {
            return userRepository.getOne(id);
        } catch (NoResultException e) {
            return null;
        }
    }

    public User findByLogin(@NotNull final String login) {
        try {
            return userRepository.findByLogin(login);
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<User> findAll() {
		return userRepository.findAll();
    }
}