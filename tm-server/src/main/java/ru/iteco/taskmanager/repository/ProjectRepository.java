package ru.iteco.taskmanager.repository;

import java.util.List;

import org.jetbrains.annotations.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.taskmanager.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    @NotNull
    @Query(value = "SELECT a FROM Project a WHERE a.name=:name")
    Project findByName(@Param("name") @NotNull final String name);

    @NotNull
    @Query(value = "SELECT a FROM Project a WHERE a.user.id=:userId")
    List<Project> findAllByOwnerId(@Param("userId") @NotNull final String userId);

    @NotNull
    @Query(value = "SELECT a FROM Project a WHERE a.name LIKE %:partOfName%")
    List<Project> findAllByPartOfName(@Param("partOfName") @NotNull final String partOfName);

    @NotNull
    @Query(value = "SELECT a FROM Project a WHERE a.description LIKE %:partOfDescription%")
    List<Project> findAllByPartOfDescription(@Param("partOfDescription") @NotNull final String partOfDescription);
}
